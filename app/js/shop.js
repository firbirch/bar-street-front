var MIN_COST_CART = 36000; // Минимальная стоимость
var DELIVERY_COST = 6000;
var basketItem = {};
$(document).ready(function () {

	// Инициализируем корзину
	initBasket();

	// Загружаем страницу и считаем там кнопочки всякое такое
	checkSumCart();

	// Смена количества
	$('.js-counter-toggle').on('click', function () {

		let $this = $(this),
			$countElem = $($this.closest('.cart-coctails__counter')).find('.input'),
			$productContainer = $(this).closest('.cart-coctails__item'),
			$price = null
		;

		let $product = $productContainer.data('product');

		let count = +$countElem.val();
		if ($this.hasClass('js-minus') && count > 25) {
			count-=25;
		} else if($this.hasClass('js-plus')) {
			count+=25;
		} else {
			return;
		}
		$countElem.val(count);
		if($price = $productContainer.find('.cart-coctails__item-price').data('price')) {
			$price = $price * parseInt($productContainer.find('.cart-coctails__sign').val());
		}
		$productContainer.find('.js-count-amount').html($price.toLocaleString('ru'));
		$productContainer.find('.js-count-amount').data('sum', $price);

		addProduct($product, $countElem.val());

		checkSumCart();
	});

	// Удаление
	$('.cart-coctails__close').on('click', function (event) {
		event.preventDefault();

		let $productContainer = $(this).closest('.cart-coctails__item');
		let $product = $productContainer.data('product');

		$productContainer.remove();

		removeProduct($product);
	});

	// Оформление

	$('.cart-total__btn--nextt').on('click', function (event) {
		event.preventDefault();
		$.ajax({
			url: '/order/product',
			data: {
				products: JSON.stringify(basketItem)
			},
			type: 'POST',
			success: function (response) {

				if(response == 'ok') {
					window.location.href = '/order/index'
				} else {
					return ;
				}
			}
		});
	});
});

function initBasket() {
	let product = null;
	$('.cart-coctails__item').each(function (index, value) {
		product = $(this).data('product');
		value = parseInt($(this).find('.cart-coctails__sign').val());
		basketItem[product] = value;
	});
}

function addProduct(product, value) {
	basketItem[product] = parseInt(value);
	checkSumCart();
}
function removeProduct(product) {
	$.ajax({
		url: '/coctails/remove',
		type: 'POST',
		data: {
			product_id: product
		},
		success: function (response) {
			console.log(response);
		}
	});
	delete basketItem[product];
	checkSumCart();
}

function checkSumCart() {
	let $price = null; let $message = $('.cart-total__min');
	let $button = $('.cart-total__btn--nextt');

	$('.cart-coctails__item').each(function (index, value) {
		$price += parseInt($(this).find('.js-count-amount').data('sum'));
	});

	$price+=DELIVERY_COST;

	if($price > MIN_COST_CART) {
		$message.hide();
		$button.removeAttr('disabled');
	} else {
		$message.show();
		$button.prop("disabled", true);
	}
	$('#jQueryCartPrice').html($price.toLocaleString('ru'));
}