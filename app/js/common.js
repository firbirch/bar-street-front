grecaptcha.ready(function() {
	grecaptcha.execute('6LeYmKQUAAAAAAWRRUDmXDAfiKmDMLRM_9DmLXC6', {action: 'submit'}).then(function(token) {
		// console.log(token);
		$('input.token').val(token);
	});
});

$(document).ready(function () {

	$('.subscribe-form__submit').on('click', function (event) {
		event.preventDefault();
		$.ajax({
			url: '/callback/subscribe',
			type: 'POST',
			data: $('.subscribe-form form').serialize(),
			success: function (response) {
				if(response == 'ok')
					return window.location.href = '/message';
				if(response == 'error')
					return $('.subscribe-form__input').css({'border': '2px solid red'});
			}
		});
		return false;
	});

	let href = window.location.hash;
	let pathname = window.location.pathname;
	let subhash = '';
	if (href && pathname == '/coctails') {
		subhash = href.substring(href.indexOf("#") + 1);
		$('.cocktails__filter-tab').removeClass('cocktails__filter-tab--active');
		$('.cocktails__filter-tab').each(function () {
			if($(this).data('servicetab') == subhash) {
				return $(this).addClass('cocktails__filter-tab--active');
			}
		});
		$.ajax({
			url: '/coctails/tab',
			type: 'POST',
			data: {
				service: subhash,
			},
			success: function (response) {
				$('#coctailsWrapper').html(response);

			}
		})
	}





	$('.popup-back__submit, .article__form-btn, .order__form-submit, .popup__submit, .event__form-btn, .js-barstreet-sale__btn-submit').on('click', function (event) {
		event.preventDefault();
		let form = $(this).closest('form');
		$(form).find('.jQueryErrors').remove();
		$.ajax({
			url: form.attr('action'),
			data: form.serialize(),
			type: 'POST',
			success: function (response) {
				if(response == 'redirect') {
					window.location.href = '/message';
				}
				else if(response == 'ok') {
					$(this).closest('.popup-back__form').addClass('_collapse');
					$('.barstreet-sale-popup-back__thanks').addClass('_active');
				} else {
					let answer = JSON.parse(response);
					let errors = answer.errors;
					$.each(errors, function (index, value) {
						if(index == 'captcha' && value == 'error') {
							$('.g-recaptcha').after('<p class="jQueryErrors">Ошибка reCaptcha</p>');
						} else {
							let input = 'input[name="CallbackForm[' + index + ']"]';
							form.find(input).before('<p class="jQueryErrors">' + value + '</p>');
						}
					});
				}
			}
		});
	});

    $('.js-landing-submit').on('click', function (event) {
        event.preventDefault();
        let form = $(this).closest('form');
        $(form).find('.jQueryErrors').remove();
        $.ajax({
            url: form.attr('action'),
            data: form.serialize(),
            type: 'POST',
            success: function (response) {
                if(response != 'redirect') {
                    let answer = JSON.parse(response);
                    let errors = answer.errors;
                    $.each(errors, function (index, value) {
                        if(index == 'captcha' && value == 'error') {
                            $('.g-recaptcha').after('<p class="jQueryErrors">Ошибка reCaptcha</p>');
                        } else {
                            let input = 'input[name="CallbackForm[' + index + ']"]';
                            form.find(input).before('<p class="jQueryErrors">' + 'incorrect value' + '</p>');
                        }
                    })
                }
                else {
                    $('.landing-thanks, .landing-overlay').addClass('_active');
                    $('.landing__callback').removeClass('_active');
                }
            }
        });
    });
});




