$(document).ready(function() {
    let items = $('.js-furniture-product');
    let itemsObj = new Object();
    let furnitureTotal = $('.js-furniture-total');

    let serviceDelivery = $('.js-service-delivery-amount'); // Обслуживание
    let delivery = $('.js-delivery-amount'); // Доставка
    let furnitureAmount = $('.js-furniture-amount'); // Сумма аренды
    let furnitureCount = $('.js-furniture-count'); // Количество мебели


    let serviceDeliveryInput = $('input[name="mebelrent[order_mebel_service]"]');
    let DeliveryInput = $('input[name="mebelrent[order_mebel_delivery]"]');
    let furnitureAmountInput = $('input[name="mebelrent[order_mebel_price]"]');
    let furnitureCountInput = $('input[name="mebelrent[order_mebel_value]"]');


    items.each(function() {
        let row = $(this).children().find('.js-furniture-product-row');
        let title = $(this).children().find('.js-furniture-product-title').text();
        let total = $(this).children().find('.js-furniture-product-total');
        let itemArr = [];
        

        row.each(function() {
            let rowObj = new Object();
            let price = $(this).children().find('.js-furniture-product-price').text().replace(/\s/g, '');
            let count = $(this).children().find('.js-furniture-product-count');
            let days = $(this).children().find('.js-furniture-product-days');
            let amount = $(this).children().find('.js-furniture-product-amount');

            count.change(function() {
                amount.text(price * count.val() * days.val());
                rowObj['price'] = price;
                rowObj['count'] = count.val();
                rowObj['days'] = days.val();
                rowObj['amount'] = +amount.text();
                if (itemArr.indexOf(rowObj) == -1) {
                    itemArr.push(rowObj);
                }
                let itemAmount = 0;
                itemArr.forEach(function (item) {
                    itemAmount += item['amount'];
                });

                total.text(itemAmount);

                let furnitureAmountVar = 0;
                let furnitureCountVar = 0

                for (key in itemsObj) {
                    itemsObj[key].forEach(function (item) {
                        furnitureAmountVar += item['amount'];
                        furnitureCountVar += +item['count'];
                    });
                }


                furnitureAmount.text(furnitureAmountVar);
                furnitureCount.text(furnitureCountVar);

                furnitureCountInput.val(furnitureCountVar);

                totalAmount(furnitureAmount, delivery, serviceDelivery);
            });

            days.change(function () {
                amount.text(price * count.val() * days.val());
                rowObj['price'] = price;
                rowObj['count'] = count.val();
                rowObj['days'] = days.val();
                rowObj['amount'] = +amount.text();
                if (itemArr.indexOf(rowObj) == -1) {
                    itemArr.push(rowObj);
                }
                
                let itemAmount = 0;
                itemArr.forEach(function(item) {
                    itemAmount += item['amount'];
                });

                total.text(itemAmount);

                let furnitureAmountVar = 0;
                let furnitureCountVar = 0

                for (key in itemsObj) {
                    itemsObj[key].forEach(function (item) {
                        furnitureAmountVar += item['amount'];
                        furnitureCountVar += +item['count'];
                    });
                }

                
                furnitureAmount.text(furnitureAmountVar);
                furnitureCount.text(furnitureCountVar);

                furnitureCountInput.val(furnitureCountVar);

                totalAmount(furnitureAmount, delivery, serviceDelivery);
            });
        });
        $('input:radio[name=delivery]').each( function() {
            $(this).change(function() {
                if ($('input:radio[name=delivery]').is(':checked') === true) {
                    if($(this).val() == 'Договорная') {
                        delivery.text('0');
                    } else {
                        delivery.text($(this).val().replace(/\s/g, ''));
                    }
                    
                }

                totalAmount(furnitureAmount, delivery, serviceDelivery);
            });

        });
        
        itemsObj[title] = itemArr;
    });

    

    $('input:checkbox[name=service-delivery-check]').change(function () {
        if ($('input:checkbox[name=service-delivery-check]').is(':checked') === true) {
            let serviceDeliveryVar = +serviceDelivery.text();
            serviceDeliveryVar += +$(this).val().replace(/\s/g, '');
            serviceDelivery.text(serviceDeliveryVar);

            
        } else if (+serviceDelivery.text() >= +$(this).val().replace(/\s/g, '')) {
            let serviceDeliveryVar = +serviceDelivery.text();
            serviceDeliveryVar -= +$(this).val().replace(/\s/g, '');
            serviceDelivery.text(serviceDeliveryVar);
        }

        totalAmount(furnitureAmount, delivery, serviceDelivery);
    }); 

    $('input[name=service-delivery]').each(function () {

        $(this).change(function () {
            if ($('input:radio[name=service-delivery]').is(':checked') === true && $('input:checkbox[name=service-delivery-check]').is(':checked') === true) {
                let serviceDeliveryVar = 0;
                serviceDeliveryVar += +$(this).val().replace(/\s/g, '') + +$('input:checkbox[name=service-delivery-check]').val().replace(/\s/g, '');
                serviceDelivery.text(serviceDeliveryVar);
            } else {
                let serviceDeliveryVar = 0;
                serviceDeliveryVar += +$(this).val().replace(/\s/g, '');
                serviceDelivery.text(serviceDeliveryVar);
            }

            totalAmount(furnitureAmount, delivery, serviceDelivery);
        });
    });

    

    function totalAmount (furnitureAmount, deliveryAmount, serviceDeliveryAmount) {

        serviceDeliveryInput.val(parseInt(serviceDeliveryAmount.text().replace(/\s/g, '')));
        DeliveryInput.val(parseInt(deliveryAmount.text().replace(/\s/g, '')));
        furnitureAmountInput.val(parseInt(furnitureAmount.text().replace(/\s/g, '')));

        $('.js-furniture-total').text(+furnitureAmount.text().replace(/\s/g, '') + +deliveryAmount.text().replace(/\s/g, '') + +serviceDeliveryAmount.text().replace(/\s/g, ''));
    }

    



    //furniture таглы калькулятора

    $('.furniture-catalog__product-calc-toggle').each(function () {
        $(this).click(function () {
            if ($(this).hasClass('furniture-catalog__product-calc-toggle--active')) {
                $(this).toggleClass('furniture-catalog__product-calc-toggle--active');
                $(this).parents().closest('.furniture-catalog__product-calc-item').removeClass('furniture-catalog__product-calc-item--active');
                $(this).parents().closest('.furniture-catalog__product-calc-item').find('.furniture-catalog__product-calc-item-inner').removeClass('furniture-catalog__product-calc-item-inner--active');
            } else {
                $(this).toggleClass('furniture-catalog__product-calc-toggle--active');
                $(this).parents().closest('.furniture-catalog__product-calc-item').addClass('furniture-catalog__product-calc-item--active');
                $(this).parents().closest('.furniture-catalog__product-calc-item').find('.furniture-catalog__product-calc-item-inner').addClass('furniture-catalog__product-calc-item-inner--active');
            }

        });
    });


})