if($('section.photomap').length > 0) {
	let locale = $('html').attr('lang');
	let places = 0;
	$.ajax({
		'url': '/photoalbum/map?locale='+locale,
		'dataType': 'json',
		'type': 'POST',
		'data': {
			crutch: 1
		},
		success: function (response) {
			places = response;
		}
	});



	mapboxgl.accessToken = 'pk.eyJ1IjoicHJvbW9zdWtzIiwiYSI6ImNqczl0eWQ3dDFxY2s0OXFwZmlteTlrZHcifQ.Z7FaX4s754k1YFUQW_CNrg';
	mapboxgl.baseApiUrl = 'https://api.mapbox.com';

	var map = new mapboxgl.Map({
		container: 'photomap',
		center: [37.6155600, 55.7522200],
		style: 'mapbox://styles/promosuks/cjs9u0tme3qqz1fn6fxlt61rw',
		zoom: 11,

	});

	map.on('load', function () {
		// Add a new source from our GeoJSON data and set the
		// 'cluster' option to true. GL-JS will add the point_count property to your source data.
		map.addSource("events", {
			type: "geojson",
			// Point to GeoJSON data. This example visualizes all M1.0+ events
			// from 12/22/15 to 1/21/16 as logged by USGS' Earthquake hazards program.
			data: places,
			cluster: true,
			clusterMaxZoom: 14, // Max zoom to cluster points on
			clusterRadius: 50 // Radius of each cluster when clustering points (defaults to 50)
		});

		map.addLayer({
			id: "clusters",
			type: "circle",
			source: "events",
			filter: ["has", "point_count"],
			paint: {
				// Use step expressions (https://docs.mapbox.com/mapbox-gl-js/style-spec/#expressions-step)
				// with three steps to implement three types of circles:
				//   * Blue, 20px circles when point count is less than 100
				//   * Yellow, 30px circles when point count is between 100 and 750
				//   * Pink, 40px circles when point count is greater than or equal to 750
				"circle-color": [
					"step",
					["get", "point_count"],
					"#ed1e79",
					100,
					"#ed1e79",
					750,
					"#ed1e79"
				],
				"circle-radius": [
					"step",
					["get", "point_count"],
					20,
					100,
					30,
					750,
					40
				]
			}
		});

		map.addLayer({
			id: "cluster-count",
			type: "symbol",
			source: "events",
			filter: ["has", "point_count"],
			layout: {
				"text-field": "{point_count_abbreviated}",
				"text-font": ["DIN Offc Pro Medium", "Arial Unicode MS Bold"],
				"text-size": 14
			},
			paint: {
				"text-color": "#ffffff"
			}
		});

		map.addLayer({
			id: "unclustered-point",
			type: "symbol",
			source: "events",
			filter: ["!", ["has", "point_count"]],
			layout: {
				"icon-image": "marker"
			}
		});
		var href = window.location.hash;
		var subhash = '';
		let state = false;
		if (href) {
			subhash = href.substring(href.indexOf("#") + 1);
			let features = places.features;
			features.forEach(function(item) {
				if (item.properties.id === subhash) {
					map.flyTo({
						center: item.geometry.coordinates,
						zoom: 15
					});
				}
			});
			map.on('idle', function() {
				if (state) return;
				if (map.loaded()) {
					features = map.queryRenderedFeatures({
						layers: ['unclustered-point']
					});
					state = true;
					placeMapInfo(features);
				}
			})
		}
	});

// Add an event listener for when a user clicks on the map
	map.on('click', 'unclustered-point', function (e) {
		placeMapInfo(e);
	});

	map.on('mouseenter', 'clusters', function () {
		map.getCanvas().style.cursor = 'pointer';
	});

	map.on('mouseleave', 'clusters', function () {
		map.getCanvas().style.cursor = '';
	});

	map.on('click', 'clusters', function (e) {
		var features = map.queryRenderedFeatures(e.point, {
			layers: ['clusters']
		});
		var clusterId = features[0].properties.cluster_id;
		map.getSource('events').getClusterExpansionZoom(clusterId, function (err, zoom) {
			if (err)
				return;

			map.easeTo({
				center: features[0].geometry.coordinates,
				zoom: zoom
			});
		});
	});

	function placeMapInfo (geoObject) {
		// Query all the rendered points in the view
		var features = map.queryRenderedFeatures(geoObject.point, {
			layers: ['unclustered-point']
		});
		let sidebar = $('.photomap__layout');

		if (!sidebar.hasClass('_active')) {
			sidebar.addClass('_active');
		}

		if (features.length) {

			var clickedPoint = features[0];
			let events = JSON.parse(clickedPoint.properties.events);
			var selectedPlaceName = events[0].placeName;
			let placeEventsArr = [];
			let listings = document.getElementById('photomap-layout-list');
			listings.innerHTML = '';
			document.getElementById('photomap-title').innerHTML = events[0].placeName;

			features.forEach(function (item) {
				let events = JSON.parse(item.properties.events);
				if (events[0].placeName === selectedPlaceName) {
					placeEventsArr.push(item);
				}
			});

			placeEventsArr.forEach(function (item) {
				placeListRender(item);
			});
			perfectScrollInit();
			createPopUp(clickedPoint, placeEventsArr.length);
		}
	}

	function placeListRender(place) {
		let events = JSON.parse(place.properties.events);
		for(let i=0; i<events.length; i++) {
			if (document.querySelector('.photomap__layout-base-list')) {
				document.querySelector('.photomap__layout-base-list').remove();
			}
			var prop = events[i];
			// Select the listing container in the HTML and append a div
			// with the class 'item' for each store
			var listings = document.getElementById('photomap-layout-list');
			var listing = listings.appendChild(document.createElement('a'));
			listing.className = 'photomap__layout-item';
			listing.id = 'photomap-item-' + prop.id;
			listing.href = prop.link;

			// Create a new link with the class 'title' for each store
			// and fill it with the store address
			var img = listing.appendChild(document.createElement('img'));
			img.src = prop.img;
			var info = listing.appendChild(document.createElement('div'));
			info.className = 'photomap__layout-item-inner';

			var itemTitle = info.appendChild(document.createElement('span'));
			itemTitle.className = 'photomap__layout-item-title';
			itemTitle.innerHTML = prop.title;
			var itemDate = info.appendChild(document.createElement('time'));
			var itemDateText = new Date(prop.date).toLocaleDateString(locale == 'en' ? 'en-US' : 'ru-RU', {
				year: 'numeric',
				month: 'long',
				day: 'numeric'
			});
			itemDate.className = 'photomap__layout-item-date';
			itemDate.setAttribute('datetime', prop.date);
			itemDate.innerHTML = itemDateText;

			var link = info.appendChild(document.createElement('a'));
			link.className = 'photomap__layout-item-link';
			link.href = prop.link;
			link.setAttribute("target", "_blank");
			link.innerHTML = locale != 'en' ? 'Смотреть отчет' : 'Watch report';
		}

	}

	function createPopUp(currentFeature, count) {
		let events = JSON.parse(currentFeature.properties.events);
		var popUps = document.getElementsByClassName('mapboxgl-popup');
		// Check if there is already a popup on the map and if so, remove it
		if (popUps[0]) popUps[0].remove();

		function declOfNum(number, titles) {
			cases = [2, 0, 1, 1, 1, 2];
			return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
		}

		let countWord =
			locale == 'en'
				? declOfNum(events.length, ['event', 'events', 'events'])
				: declOfNum(events.length, ['мероприятие', 'мероприятия', 'мероприятий']);

		var popup = new mapboxgl.Popup({
			closeOnClick: false
		})
			.setLngLat(currentFeature.geometry.coordinates)
			.setHTML('<h3>' + events[0].placeName + '</h3>' +
				'<h4>' + events.length + ' ' + countWord + '</h4>')
			.addTo(map);
	}

	function perfectScrollInit() {
		const container = document.querySelector('#photomap-layout-list');
		let containerHeight = document.querySelector('.photomap__layout').offsetHeight - document.querySelector('.photomap__layout-base-title').offsetHeight - document.querySelector('.photomap__layout-bottom').offsetHeight;
		container.style.height = containerHeight + 'px';
		const ps = new PerfectScrollbar(container, {
			suppressScrollX: true
		});
	}
}