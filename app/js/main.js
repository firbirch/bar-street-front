/* frameworks */
// =include ../../node_modules/jquery/dist/jquery.js
/* libs */
// =include ../../node_modules/owl.carousel/dist/owl.carousel.js
// =include ../../node_modules/readmore-js/readmore.js
// =include ../../node_modules/inputmask/dist/jquery.inputmask.bundle.js
// =include ../../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js
// =include ../../node_modules/lazysizes/lazysizes.js
// =include ../../node_modules/vanilla-lazyload/dist/lazyload.js
// =include ../../node_modules/mapbox-gl/dist/mapbox-gl.js
// =include ../../node_modules/perfect-scrollbar/dist/perfect-scrollbar.js
// =include ../../node_modules/select2/dist/js/select2.full.js
// =include ../../node_modules/slick-carousel/slick/slick.min.js
/* files */
// =include furniture-calc.js
// =include ../libs/linkedSelects/linkedSelect.js
// =include photomap.js
// =include shop.js
// =include common.js


$(document).ready(function () {
    if(!localStorage.getItem('ConfirmationOfAge')=='1') {
        $('.eighteen-plus-message').removeClass('hidden');
        $('.overlay-eighteen-plus').removeClass('hidden');
        $('body').addClass('popup-open');
	}

    //up btn
    if($('.body').hasClass('landing-body')) {
    	$('.landing-desc').find('.landing-desc__inner').addClass('_visible');
    }

    $('.service-media__slider').owlCarousel({
        loop: true,
        nav: false,
        dots: true,
        items: 1,
        animateOut: 'fadeOut'
    });

    if($(window).width()<991) {
    	$('.main-nav__list').not( '.main-nav__list--links' ).append($('.main-contacts__eng-ver'));
        $('.main-contacts__eng-ver').wrap("<li class='main-nav__item'></li>");
	}

    new LazyLoad({
        elements_selector: ".lazyload, .lazy-image, .lazy"
    });


    //up btn
	let upBtnShow = 400;
	$(window).scroll(function () {
		if ($(this).scrollTop() > upBtnShow) $('.up-btn').removeClass('_hidden');
		else $('.up-btn').addClass('_hidden');
	});
	$('.up-btn').click(function () {
		$('body, html').animate({
			scrollTop: 0
		}, 1000);
		$(this).removeClass('_hidden');
	});

	//Sliders
	$('.team-projects__slider').owlCarousel({
		loop: true,
		margin: 5,
		nav: true,
		dots: false,
		navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
			'<svg><use xlink:href="#icon-nav" /></svg>'
		],
		items: 1,
		lazyLoad: true,
		animateOut: 'fadeOut',
	});

	$('.hero-slider').owlCarousel({
		loop: true,
		margin: 0,
		nav: true,
		navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
			'<svg><use xlink:href="#icon-nav" /></svg>'
		],
		items: 1,
		lazyLoad: true,
		animateOut: 'fadeOut',
		responsive: {
			0: {
				dots: false,
			},

			992: {
				dots: true
			}
		}
	});
	$('.clients-slider__inner').owlCarousel({
		loop: true,
		nav: true,
		dots: false,
		//margin: 10,
		items: 3,
		navContainer: '.clients-slider__navigation',
		navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
			'<svg><use xlink:href="#icon-nav" /></svg>'
		],
		responsive: {
			992: {
				items: 6
			}
		}
	});
	$('.service-catalog__slider').owlCarousel({
		loop: true,
		margin: 10,
		nav: true,
		dots: false,
		navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
			'<svg><use xlink:href="#icon-nav" /></svg>'
		],
		responsive: {
			0:{
				items: 1
			},
			768:{
				items: 2
			}

		}
	});

	if ($(window).width() >= 992) {
		$(".service-catalog__slider").trigger("destroy.owl.carousel").removeClass("owl-carousel");
	}
	$('.service-clients__clients-slider').owlCarousel({
		loop: true,
		margin: 30,
		items: 1,
		nav: true,
		lazyLoad: true,
		dots: false,
		navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
			'<svg><use xlink:href="#icon-nav" /></svg>'
		],
		autoWidth: true,
		navContainerClass: 'service-clients__clients-slider-nav',
		responsive: {
			0:{
				items: 2
			},
			768:{
				items: 1,
				autoWidth: false
			}
		}
	});
	$('.service-clients__reviews-slider').owlCarousel({
		loop: true,
		margin: 10,
		items: 1,
		nav: true,
		dots: false,
		navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
			'<svg><use xlink:href="#icon-nav" /></svg>'
		],
		navContainerClass: 'service-clients__reviews-slider-nav'
	});

    $('.need__slider').owlCarousel({
        // loop: true,
        margin: 30,
        items: 3,
        nav: true,
        lazyLoad: true,
        dots: false,
        navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
            '<svg><use xlink:href="#icon-nav" /></svg>'
        ],
        navContainerClass: 'service-clients__need-slider-nav',
        responsive: {
            0:{
                items: 1
            },
            576:{
                items: 2
            },
            992:{
                items: 3
            }
        }
    });


	$('.furniture-catalog__product-slider').owlCarousel({
		loop: true,
		margin: 10,
		items: 1,
		nav: true,
		dots: false,
		navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
			'<svg><use xlink:href="#icon-nav" /></svg>'
		]
	});

	if($(window).width()>575) {
		$('.landing-intro__video').append("<video autoplay muted loop playsinline poster='/img/landing/Photo.jpg'><source src='/img/landing/final.mp4' type='video/mp4'></video>");
	}

    $('.landing-bars__slider').owlCarousel({
		loop: true,
        margin: 30,
        items: 1,
        nav: true,
        lazyLoad: true,
        dots: true,
        navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
            '<svg><use xlink:href="#icon-nav" /></svg>'
        ],
    });

    $('.landing-clients__slider').owlCarousel({
        loop: true,
        margin: 25,
        items: 4,
        slideBy: 4,
        nav: true,
        lazyLoad: true,
        dots: false,
        navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
            '<svg><use xlink:href="#icon-nav" /></svg>'
        ],
        responsive: {
            576:{
                items: 6,
                margin: 50,
                slideBy: 4
            },
            768:{
                items: 7,
				margin: 50,
                slideBy: 4
            },
            992:{
                items: 7,
                margin: 80,
                slideBy: 4
            },
            1200:{
                items: 8,
                margin: 80,
                slideBy: 4
            }
        }
    });
    $('.landing-feedbacks__slider').owlCarousel({
        loop: true,
        margin: 50,
        items: 1,
        nav: true,
        lazyLoad: true,
        dots: true,
        navText: ['<svg><use xlink:href="#icon-nav" /></svg>',
            '<svg><use xlink:href="#icon-nav" /></svg>'
        ],
        responsive: {
            768:{
                items: 3,
				dots: false,
                nav: false
            }
        }
    });

    $('.landing-nav__item').click(function(){
		if($(window).width()<575) {
			$('.menu').hide();
            $('.landing-body').removeClass('_no-scroll');
		}
	});

    $('.landing-overlay, .js-landing-popup-close').click(function(){
    	$('.landing-overlay, .landing-photos-popup, .js-landing-popup').removeClass('_active');
        $('.landing-body').removeClass('_no-scroll');
	});

    $('.landing__callback svg').click(function () {
        $('.landing__callback').removeClass('_active');
    });

    $('.landing-nav__toggle').click(function(){
        $('.landing-body').addClass('_no-scroll');
    });

    $('.js-photos-info').click(function(){
    	$('.landing-body').addClass('_no-scroll');
		$(this).next('.landing-photos-popup').addClass('_active');
        $('.landing-overlay').addClass('_active');
        if($(this).next('.js-landing-popup').length > 0) {
            $(this).next('.js-landing-popup').find('.landing-photos-popup__slider').owlCarousel({
				items: 1,
				nav: true,
				lazyLoad: true,
                navText: ['<svg class="landing-photos-popup__arrow-left"><use xlink:href="#icon-nav" /></svg>',
                    '<svg class="landing-photos-popup__arrow-right"><use xlink:href="#icon-nav" /></svg>'
                ],
				loop: true,
                dots: false
            });
        }
        else {
        	$(this).next('.landing-photos-popup').addClass('_map-open');
            map.resize();
            map.setLayoutProperty('settlement-subdivision-label', 'text-field', ['get', 'name_en']);
            map.setLayoutProperty('settlement-label', 'text-field', ['get', 'name_en']);
            map.setLayoutProperty('road-label', 'text-field', ['get', 'name_en']);
            map.setLayoutProperty('poi-label', 'text-field', ['get', 'name_en']);
		}
    });

    $('.landing-nav__close').click(function(){
        $('.landing-body').removeClass('_no-scroll');
    });
    $('.landing-nav__callback-link').click(function(){
        $('.landing__callback').addClass('_active');
    });

    $('.js-landing-scroll').click(function() {
		$('body, html').animate({
			scrollTop: $($(this).attr('href')).offset().top - 50
		}, 1000);
	});

    $('.js-landing-minus').click(function(){
        let val = Number.parseInt($(this).next('.landing-cost__input').val()) - 1;
        if(val > -1) {
            $(this).next('.landing-cost__input').val(val);
		}
    });

    $('.js-landing-plus').click(function(){
        let val = Number.parseInt($(this).prev('.landing-cost__input').val()) + 1;
        $(this).prev('.landing-cost__input').val(val);
    });

    $('.landing-additional__tabs-links').click(function(){
    	if($(window).width()<767) {
            $('.landing-additional__tabs-links').toggleClass('_active');
		}
	});


	//popups
	$('.popup__comment-label').click(function(event) {
		event.preventDefault();
		if($(this).hasClass('popup__comment-open')) {
			$(this).removeClass('popup__comment-open');
			$('.popup__comment-area').slideUp(200);
		} else {
			$(this).addClass('popup__comment-open');
			$('.popup__comment-area').slideDown(200);
		}

	});

	if($(window).width() < 768) {
		$('.menu-open-toggle').click(function () {
			$('.menu').show();
		});
		$('.menu-close-toggle').click(function () {
			$('.menu').hide();
		});
	} else {
		$('.menu-open-toggle').click(function () {
			$('.menu').toggleClass('_active');
			$('.overlay').removeClass('hidden');
		});
		$('.menu-close-toggle').click(function () {
			$('.menu').toggleClass('_active');
			$('.overlay').addClass('hidden');
		});
	}
	$('.mobile-overlay').click(function () {
		$('#mobile-nav').addClass('hidden');
		$('.mobile-overlay').addClass('hidden');
		$('.popup-how-work').addClass('hidden');
	});
	$('.overlay').click(function () {
		$('.popup').addClass('hidden');
		$('.overlay').addClass('hidden');
		$('.popup-how-work').addClass('hidden');
		$('.popup__inner').hide();
		$('body').removeClass('popup-open');
		$('.menu').removeClass('_active');
		$('.menu-contacts').removeClass('_active');
	});
	$('.main-header__contacts-back').click(function (event) {
		event.preventDefault();
		$('.popup-back').removeClass('hidden');
		$('.popup-back__overlay').removeClass('hidden');
		$('.popup-back__form').slideDown(200);
	});
	$('.info__item-link--price').click(function (event) {
		event.preventDefault();
		$('.popup-back').removeClass('hidden');
		$('.popup-back__overlay').removeClass('hidden');
		$('.popup-back__form').slideDown(200);
	});

	$('.js-callback-toggle').on('mouseup',function (event) {
		event.preventDefault();
		$('.popup-back').toggleClass('hidden');
	});

	$('body').click(function (event) {
		if (!$(event.target).closest('.popup-back').length && !$(event.target).is('.popup-back') && !$(event.target).is('.js-callback-toggle')) {
			$(".popup-back").addClass('hidden');
		}
	});

	$(document).scroll(function() {
		if($(document).scrollTop() > 300) {
			$(".popup-back").addClass('hidden');
		}
		if($('.body').hasClass('landing-body')) {
            if(($(document).scrollTop() + $(window).height() > $('.landing-how-works').offset().top) && (!$('.landing-how-works').hasClass('_visible'))) {
                $('.landing-how-works').addClass('_visible');
            }
		}
	});

	if($(window).width() < 768) {
		$('.js-contacts-toggle').click(function () {
			$('.menu-contacts').show();
		});
		$('.menu-close-contacts-toggle').click(function () {
			$('.menu-contacts').hide();
		});
	} else {
		$('.js-contacts-toggle').click(function () {
			$('.menu-contacts').toggleClass('_active');
			$('.overlay').removeClass('hidden');
		});
		$('.menu-close-contacts-toggle').click(function () {
			$('.menu-contacts').toggleClass('_active');
			$('.overlay').addClass('hidden');
		});
	}

	$('.popup-back__close').click(function (event) {
		event.preventDefault();
		$('.popup-back').addClass('hidden');
	});

	//TODO: удалить для бека

	$('.popup__close').click(function (event) {
		event.preventDefault();
		$('.overlay').addClass('hidden');
		$('body').removeClass('popup-open');
		$('.popup').addClass('hidden');
		$('.popup__inner').hide();
		$('.popup-back').addClass('hidden');
	});

	$(document).keyup(function (e) {
		if (e.keyCode === 27) {
			$('.popup-back').addClass('hidden');
			$('.popup-back__form').hide();
			$('.popup').addClass('hidden');
			$('body').removeClass('popup-open');
		}
	});

	$('.how-work-toggle').click(function () {
		$('body').addClass('popup-open');
		$('.popup-how-work').removeClass('hidden');
		$('.popup-how-work__overlay').removeClass('hidden');
		$('.popup-how-work__inner').removeClass('hidden');
		$('.popup-how-work__inner').slideDown(200);

	});

	$('.service__intro-btn, .js-callback-form-toggle, .consultation__callback-link').click(function () {
		let title = $(this).attr('data-title');
		$('.popup-order-service__title').html(title);
		$('body').addClass('popup-open');
		$('.popup-order-service').removeClass('hidden');
		$('.popup-order-service__overlay').removeClass('hidden');
		$('.popup-order-service__inner').slideDown(200);
	});

	$('.consultation__callback-link').click(function () {
		$('.popup-order-service__title').html('Заказать обратный звонок');
		$('body').addClass('popup-open');
		$('.popup-order-service').removeClass('hidden');
		$('.popup-order-service__overlay').removeClass('hidden');
		$('.popup-order-service__inner').slideDown(200);
	});

	$('.question-popup-toggle').click(function () {
		$('body').addClass('popup-open');
		$('.popup-question').removeClass('hidden');
		$('.popup-question__overlay').removeClass('hidden');
		$('.popup-question__inner').slideDown(200);
	});


	$('.furniture-catalog__product-calc-item-input').each(function() {
		$(this).click(function() {
			if($(this).val() == "0")
            	$(this).val("");
		});
		$(this).attr("pattern", "[0-9]*").attr("inputmode", "numeric");
		$(this).parent().append('<button type="button" class="furniture-catalog__product-calc-item-input-minus">-</button>');
        $(this).parent().append('<button type="button" class="furniture-catalog__product-calc-item-input-plus">+</span>');
	});

    $('.furniture-catalog__product-calc-item-input').on('focusout', function() {
    	if($(this).val() == "")
    		$(this).val("0");
	});

	$('.furniture-catalog__product-calc-item-input-minus').click(function() {
		if($(this).parent().find('.furniture-catalog__product-calc-item-input').val() == "")
            $(this).parent().find('.furniture-catalog__product-calc-item-input').val("0");
		else {
            let val = Number.parseInt($(this).parent().find('.furniture-catalog__product-calc-item-input').val()) - 1;
            if (val > -1) {
                $(this).parent().find('.furniture-catalog__product-calc-item-input').val(val);
            }
        }
        $(this).parent().find('.furniture-catalog__product-calc-item-input').trigger('change');
	});

    $('.furniture-catalog__product-calc-item-input-plus').click(function() {
        if($(this).parent().find('.furniture-catalog__product-calc-item-input').val() == "")
            $(this).parent().find('.furniture-catalog__product-calc-item-input').val("1");
        else {
            let val = Number.parseInt($(this).parent().find('.furniture-catalog__product-calc-item-input').val()) + 1;
            $(this).parent().find('.furniture-catalog__product-calc-item-input').val(val);
        }
        $(this).parent().find('.furniture-catalog__product-calc-item-input').trigger('change');
    });

	$('.furniture-catalog__product-order-btn').each(function() {
		$(this).click(function() {
			let title = $(this).attr('data-title');
			$('.popup-order-service__title').html('Заказать ' + title);
			$('body').addClass('popup-open');
			$('.popup-order-service').removeClass('hidden');
			$('.popup-order-service__overlay').removeClass('hidden');
			$('.popup-order-service__inner').slideDown(200);
		});
	});

	$('.service__catalog-btn').each(function () {
		$(this).click(function () {
			let title = $(this).attr('data-title');
			$('.popup-order-service__title').html('Заказать ' + title);
			$('body').addClass('popup-open');
			$('.popup-order-service').removeClass('hidden');
			$('.popup-order-service__overlay').removeClass('hidden');
			$('.popup-order-service__inner').slideDown(200);
		});
	});

	//contacts number switch animation
	$('.main-header__contacts-town--spb').click(function (event) {
		event.preventDefault();
		$('.main-header__contacts-town--spb').addClass('main-header__contacts-town--active');
		$('.main-header__contacts-town--msk').removeClass('main-header__contacts-town--active');
		$('.main-header__contacts-phone--msk').removeClass(
			'main-header__contacts-phone--active  fadeIn');
		$('.main-header__contacts-phone--msk').addClass(
			'main-header__contacts-phone--hide  fadeOut');
		$('.main-header__contacts-phone--spb').addClass(
			'main-header__contacts-phone--active  fadeIn');
		$('.main-header__contacts-phone--spb').removeClass(
			'main-header__contacts-phone--hide  fadeOut');
	});
	$('.main-header__contacts-town--msk').click(function (event) {
		event.preventDefault();
		$('.main-header__contacts-town--msk').addClass('main-header__contacts-town--active');
		$('.main-header__contacts-town--spb').removeClass('main-header__contacts-town--active');
		$('.main-header__contacts-phone--spb').removeClass(
			'main-header__contacts-phone--active  fadeIn');
		$('.main-header__contacts-phone--spb').addClass(
			'main-header__contacts-phone--hide  fadeOut');
		$('.main-header__contacts-phone--msk').addClass(
			'main-header__contacts-phone--active  fadeIn');
		$('.main-header__contacts-phone--msk').removeClass(
			'main-header__contacts-phone--hide  fadeOut');
	});

	$('.news__more-toggle').on('click', function () {

		let shown = 0, toShow = $(this).data('to-show'), items = $(".news__item.hidden");
		items.each(function () {
			if (shown++ < toShow) {
				$(this).removeClass('hidden');
			}
		});
		if(items.length == 1 || items.length == 0) {
			$(this).hide();
		}
	});





	// feedShare
	var $shareBtn = $('.news__share');
	$shareBtn.on('click', function () {
		var $this = $(this);
		if ($this.hasClass('news__share--active')) {
			$this.removeClass('news__share--active').find('.js-feed-share-dropdown').fadeOut(300);
		} else {
			$this.addClass('news__share--active').find('.js-feed-share-dropdown').fadeIn(300);
		}
	});

    const Share = {
        vk: function (purl, ptitle, pimg, text) {
            var url = 'http://vkontakte.ru/share.php?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&image=' + encodeURIComponent(pimg);
            url += '&noparse=true';
            Share.open(url);
        },
        facebook: function (purl, ptitle, pimg, text) {
            var url = 'https://www.facebook.com/sharer.php?s=100';
            url += '&p[title]=' + encodeURIComponent(ptitle);
            url += '&p[summary]=' + encodeURIComponent(text);
            url += '&p[url]=' + encodeURIComponent(purl);
            url += '&p[images][0]=' + encodeURIComponent(pimg);
            Share.open(url);
        },
        ok: function (purl, ptitle, pimg) {
            var url = 'https://connect.ok.ru/offer?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&imageUrl=' + encodeURIComponent(pimg);
            Share.open(url);
        },
        open: function (url) {
            window.open(url, '_blank');
        }
    };

	$('.js-share').on('click', function (e) {
		e.preventDefault();
		Share[$(this).data('target')]($(this).data('url'), $(this).data('title'), $(this).data('img'), $(this).data('text'));
	});

	$('#contacts-spb').click(function(e) {
		e.preventDefault;
		$('#contacts-spb').addClass('contacts__tab--active');
		$('#contacts-msk').removeClass('contacts__tab--active');
		$('.contacts__inner--msk').hide();
		$('.contacts__inner--spb').show();
	});

	$('#contacts-msk').click(function (e) {
		e.preventDefault;
		$('#contacts-msk').addClass('contacts__tab--active');
		$('#contacts-spb').removeClass('contacts__tab--active');
		$('.contacts__inner--spb').hide();
		$('.contacts__inner--msk').show();
	});

	$('.another__toggle').on('click', function (e) {
		e.preventDefault();

		if ($(this).hasClass('another__toggle--active')) {
			return;
		}

		var $prev = $(this).parents('ul').find('.another__toggle--active');
		$prev.removeClass('another__toggle--active');
		$(this).addClass('another__toggle--active');

		$($prev.attr('href')).removeClass('another__tab-list--active');
		$($(this).attr('href')).addClass('another__tab-list--active');
	});

	$('.service__question-tab').on('click', function (e) {
		e.preventDefault();

		if ($(this).hasClass('service-bar__question-tab--active')) {
			return;
		}

		var $prev = $(this).parents('ul').find('.service-bar__question-tab--active');
		$prev.removeClass('service-bar__question-tab--active');
		$(this).addClass('service-bar__question-tab--active');

		$($prev.attr('href')).removeClass('service-bar__question-answer-item--active');
		$($(this).attr('href')).addClass('service-bar__question-answer-item--active');
	});

	$('.go_to').click(function () {
		var scroll_el = $(this).attr('href');
		console.log(scroll_el);
		if ($(scroll_el).length != 0) {
			$('html, body').animate({scrollTop: $(scroll_el).offset().top}, 500);
		}
		return false;
	});


	var selectIng = 0;
	var selectTab = 0;
	// Стилизация select

	$('.select').each(function () {
		// Variables
		var $this = $(this),
			selectOption = $this.find('option'),
			selectOptionLength = selectOption.length,
			selectedOption = selectOption.filter(':selected');

		$this.hide();
		// Wrap all in select box
		$this.wrap('<div class="cocktails__filter-select-wrapper"></div>');
		// Style box
		$('<div>', {
			class: 'cocktails__filter-select',
			text: selectedOption[0].text
		}).insertAfter($this);

		var selectGap = $this.next('.cocktails__filter-select');
		// Add ul list
		$('<ul>', {
			class: 'cocktails__filter-select-list'
		}).insertAfter(selectGap);

		var selectList = selectGap.next('.cocktails__filter-select-list');
		// Add li - option items
		for (var i = 0; i < selectOptionLength; i++) {
			$('<li>',
				{
					class: 'cocktails__filter-select-item',
					html: $('<span>', {
						text: selectOption.eq(i).text()
					}),
					value: selectOption.eq(i).text()
				})
				.attr('data-value', selectOption.eq(i).val())
				.appendTo(selectList);
		}
		// Find all items
		var selectItem = selectList.find('li');
		$(selectItem[0]).hide();

		selectList.slideUp(0);
		selectGap.on('click', function () {
			if (!$(this).hasClass('cocktails__filter-select--active')) {
				$(this).addClass('cocktails__filter-select--active');
				selectList.show();
				var hiddenItems = selectItem.filter(':hidden');

				selectItem.on('click', function () {
					hiddenItems.each(function () {
						$(this).show();
					})

					$(this).hide();

					var chooseItem = $(this).attr('value');


					$('select').val(chooseItem).attr('selected', 'selected');
					selectGap.text($(this).find('span').text());

					selectIng = $(this).data('value');
					//console.log(selectIng);

					getPages();



					selectList.hide();

					selectGap.removeClass('cocktails__filter-select--active');

				});

			} else {
				$(this).removeClass('cocktails__filter-select--active');


				selectList.hide();
			}
		});

	});

	$('.landing-cost-select-bar-type').select2({
        placeholder: 'Choose bar type',
        minimumResultsForSearch: -1
	});

    $('.landing-cost-select-bar').select2({
        placeholder: 'Choose bar',
        minimumResultsForSearch: -1
    });

	//furniture табы описания продукта

	$('.js-char').each(function() {
		$(this).click(function (e) {
			e.preventDefault();
			$(this).parent().find('.js-desc').removeClass('furniture-catalog__product-tabs-toggle--active');
			$(this).parent().find('.furniture-catalog__product-tabs-content').hide();
			$(this).parent().find('.furniture-catalog__product-tabs-char').show();
			$(this).addClass('furniture-catalog__product-tabs-toggle--active');
		});
	});

	$('.js-desc').each(function() {
		$(this).click(function (e) {
			e.preventDefault();
			$(this).parent().find('.js-char').removeClass('furniture-catalog__product-tabs-toggle--active');
			$(this).parent().find('.furniture-catalog__product-tabs-content').show();
			$(this).parent().find('.furniture-catalog__product-tabs-char').hide();
			$(this).addClass('furniture-catalog__product-tabs-toggle--active');
		});
	});



	//cocktails табы фильтра

	$('.cocktails__filter-tab').click(function (e) {
		e.preventDefault();

		if ($(this).hasClass('cocktails__filter-tab--active')) {
			return;
		}

		var $prev = $(this).parents('ul').find('.cocktails__filter-tab--active');
		$prev.removeClass('cocktails__filter-tab--active');
		$(this).addClass('cocktails__filter-tab--active');

		// $($prev.attr('href')).removeClass('active');
		// $($(this).attr('href')).addClass('active');
	});

	// $('.service-clients__reviews-content').readmore({
	// 	speed: 75,
	// 	maxHeight: ($(window).width() > 991)&&($(window.width()< 1200)) ? 350 : 240,
	// 	moreLink: '<a href = "#" class="service-clients__reviews-more">Показать полностью</a>',
	// 	lessLink: '<a href = "#" class="service-clients__reviews-more">Скрыть</a>'
	// });

	//mask phone

	$('.phone-input').inputmask("+9 (999) 999-99-99", {"placeholder": "+7                "});

	//fancybox

	$('[data-fancybox]').fancybox({
		mobile: {
			clickContent: "close",
			clickSlide: "close"
		},
		buttons: [
			"zoom",
			//"share",
			// "slideShow",
			//"fullScreen",
			//"download",
			"close"
		],
	});

	function flyInCart(id) {
		var image = $('#coct' + id),
			copy = image.clone(),
			outer = $('<div>', {
				class: 'fly'
			}).appendTo('body');
		outer.html(copy).css({
			top: image.offset().top,
			left: image.offset().left
		}).show().animate({
			left: $('.cocktails__cart-toggle').offset().left + 2,
			top: $('.cocktails__cart-toggle').offset().top - 30,
			width: '42px',
			height: '42px'
		}, 500, function () {
			$(this).animate({
				paddingTop: '50px'
			}, 300, function () {
				$(this).remove();
			});
		});

	}


	// BACKEND

	$('.cocktails__filter-tab').on('click', function () {
		selectTab = $(this).data('service');
		getPages();
	});
	function getPages() {
		$.ajax({
			url: '/coctails/filter',
			type: 'POST',
			data: {
				service: selectTab,
				ingredient: selectIng
			},
			success: function (response) {
				$('#coctailsWrapper').html(response);
			}
		})
	}

	/*======================================================================
	=            Скрытие сайдбара на странице карты фотоотчетов            =
	======================================================================*/
	let photomapLayout = $('.photomap__layout');
	let photomapLayoutToggle = $('.js-layout-hide-toggle');
	if ($(window).width() < 768) {
		photomapLayout.removeClass('_active');
		photomapLayoutToggle.removeClass('_active');
	}

	photomapLayoutToggle.click(function () {
		photomapLayout.toggleClass('_active');
		photomapLayoutToggle.toggleClass('_active');
	});

	$('.barstreet-sale__offers-tab-title').click(function(){
        $('.barstreet-sale__offers-tab-title').removeClass('_active');
        $(this).addClass('_active');
	});

	/*=====  End of Скрытие сайдбара на странице карты фотоотчетов  ======*/
});
$(document).on('click', '.cocktails__product-order', function () {
	if (!$(this).hasClass("cocktails__product-order--added")) {
		$(this).parent('.cocktails__product').find('div.cocktails__product-img img');
		$(this).addClass("cocktails__product-order--added").text("ЗАКАЗАНО");
		$(".cocktails__cart-toggle span").each(function () {
			var count = parseFloat($(this).text().replace(' коктейлей', '')) + 1;
			$(this).text("" + count + " " + declens(count, ['коктейль', 'коктейля', 'коктейлей']));
		});
		var id = $(this).data('id');

		$.ajax({
			url: '/coctails/add',
			type: 'POST',
			data: {
				product_id: id
			},
			success: function (response) {
				console.log(response);
			}
		});
		flyInCart(id);
	}

	function flyInCart(id) {
		var image = $('#coct' + id),
			copy = image.clone(),
			outer = $('<div>', {
				class: 'fly'
			}).appendTo('body');
		outer.html(copy).css({
			top: image.offset().top,
			left: image.offset().left
		}).show().animate({
			left: $('.cocktails__cart-toggle').offset().left + 2,
			top: $('.cocktails__cart-toggle').offset().top - 30,
			width: '42px',
			height: '42px'
		}, 500, function () {
			$(this).animate({
				paddingTop: '50px'
			}, 300, function () {
				$(this).remove();
			});
		});
	}
	function declens (number, titles) {
		cases = [2, 0, 1, 1, 1, 2];
		return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
	}
});

$('.js-clients-more').click(function() {
	$('.clients__item').css('display', 'flex');
	$(this).css('display', 'none');
});

$('.eighteen-plus-message__close').on('click', function () {
    localStorage.setItem('ConfirmationOfAge','1');
    $('.eighteen-plus-message').addClass('hidden');
    $('.overlay-eighteen-plus').addClass('hidden');
    $('body').removeClass('popup-open');
});

$('.landing-additional__tabs-link').on('click',function() {
    $('.landing-additional__tabs-link').removeClass('_active');
    $(this).addClass('_active');
});

function landingTabs(tabs) {
    $('.landing-additional__tabs-content').removeClass('_active');
	$(tabs).addClass('_active');
}

$('.barstreet-sale__table-title').click(function(){
	if($(window).width()<767) {
        $(this).find('.barstreet-sale__table-title-arrow').toggleClass('_open');
        $(this).next('table').toggleClass('_open');
	}
});

$('.barstreet-sale__button').click(function(){
	$('.barstreet-sale-popup-back, .barstreet-sale-overlay').addClass('_active');
});

$('.barstreet-sale-popup-close, .barstreet-sale-overlay').click(function(){
    $('.barstreet-sale-popup-back, .barstreet-sale-overlay').removeClass('_active');
});

function barstreet_sale_tab(num, btn) {
    $('.barstreet-sale__offers-tab').removeClass('_active').fadeOut(300);
    $('#offers_tab' + num).addClass('_active').fadeIn(300);
    $('input.js-october-type-selector-input').val('Заявка на ' + btn.html());
}
