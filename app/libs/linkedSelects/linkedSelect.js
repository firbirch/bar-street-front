/*
wwww.tigir.com (РґР°С‚Р° РїРѕСЃР»РµРґРЅРµР№ РјРѕРґРёС„РёРєР°С†РёРё - 30.11.2007)
Р‘РёР±Р»РёРѕС‚РµРєР° linkedselect.js РёР· СЃС‚Р°С‚СЊРё "Javascript SELECT - РґРёРЅР°РјРёС‡РµСЃРєРёРµ СЃРїРёСЃРєРё" - http://www.tigir.com/linked_select.htm

syncList - "РєР»Р°СЃСЃ" СЃРІСЏР·Р°РЅРЅС‹С… СЃРїРёСЃРєРѕРІ
*/
function syncList(){} //РћРїСЂРµРґРµР»СЏРµРј С„СѓРЅРєС†РёСЋ РєРѕРЅСЃС‚СЂСѓРєС‚РѕСЂ

//РћРїСЂРµРґРµР»СЏРµРј РјРµС‚РѕРґС‹

//РњРµС‚РѕРґ sync() - РїСЂРёРЅРёРјР°РµС‚ СЃРїРёСЃРѕРє РёР· Р·РЅР°С‡РµРЅРёР№ Р°С‚СЂРёР±СѓС‚РѕРІ id СЌР»РµРјРµРЅС‚РѕРІ SELECT, РѕР±СЂР°Р·СѓСЋС‰РёС… СЃРІСЏР·Р°РЅРЅС‹Р№ СЃРїРёСЃРѕРє Рё Р·Р°РїСѓСЃРєР°РµС‚ РёС… СЃРёРЅС…СЂРѕРЅРёР·Р°С†РёСЋ
syncList.prototype.sync = function()
{
    //РџРµСЂРµР±РёСЂР°РµРј Р°СЂРіСѓРјРµРЅС‚С‹ (id СЌР»РµРјРµРЅС‚РѕРІ SELECT) Рё РЅР°Р·РЅР°С‡Р°РµРј СЃРѕР±С‹С‚РёСЏРј onChange СЃРµР»РµРєС‚РѕРІ, СЃ СЃРѕРѕС‚РІРµС‚СЃС‚РІСѓСЋС‰РёРјРё id, С„СѓРЅРєС†РёСЋ-РѕР±СЂР°Р±РѕС‚С‡РёРє.
    //Р’ РєР°С‡РµСЃС‚РІРµ РѕР±СЂР°Р±РѕС‚С‡РёРєР° РІС‹СЃС‚СѓРїР°РµС‚ РІС‚РѕСЂРѕР№ РјРµС‚РѕРґ РѕР±СЉРµРєС‚Р° syncList - _sync (РЅР°РїСЂСЏРјСѓСЋ РµРіРѕ РІС‹Р·С‹РІР°С‚СЊ РЅРµ РЅСѓР¶РЅРѕ)
    //РћР±СЂР°Р±РѕС‚С‡РёРє РЅР°Р·РЅР°С‡Р°РµС‚СЃСЏ РІСЃРµРј СЌР»РµРјРµРЅС‚Р°Рј SELECT РєСЂРѕРјРµ РїРѕСЃР»РµРґРЅРµРіРѕ РІ СЃРїРёСЃРєРµ Р°СЂРіСѓРјРµРЅС‚РѕРІ, С‚.Рє. РїРѕСЃР»РµРґРЅРёР№ РЅРµ РІР»РёСЏРµС‚ РЅРё РЅР° РєР°РєРѕР№ РґСЂСѓРіРѕР№ СЌР»РµРјРµРЅС‚ SELECT Рё СЃ РЅРёРј РЅРµ РЅСѓР¶РЅРѕ СЃРёРЅС…СЂРѕРЅРёР·РёСЂРѕРІР°С‚СЊСЃСЏ.
    for (var i=0; i < arguments.length-1; i++)	document.getElementById(arguments[i]).onchange = (function (o,id1,id2){return function(){o._sync(id1,id2);};})(this, arguments[i], arguments[i+1]);
    document.getElementById(arguments[0]).onchange();//Р·Р°РїСѓСЃРєР°РµРј РѕР±СЂР°Р±РѕС‚С‡РёРє onchange РїРµСЂРІРѕРіРѕ СЃРµР»РµРєС‚Р°, С‡С‚РѕР±С‹ РїСЂРё Р·Р°РіСЂСѓР·РєРµ СЃС‚СЂР°РЅРёС†С‹ Р·Р°РїРѕР»РЅРёС‚СЊ РґРѕС‡РµСЂРЅРёРµ СЃРµР»РµРєС‚С‹ Р·РЅР°С‡РµРЅРёСЏРјРё.
}
//СЃР»СѓР¶РµР±РЅС‹Р№ РјРµС‚РѕРґ _sync - СЃСЂР°Р±Р°С‚С‹РІР°РµС‚ РїСЂРё СЃРјРµРЅРµ РІС‹Р±СЂР°РЅРЅРѕРіРѕ СЌР»РµРјРµРЅС‚Р° РІ С‚РµРєСѓС‰РµРј (СЃС‚Р°СЂС€РµРј) СЌР»РµРјРµРЅС‚Рµ SELECT (РїРѕ РµРіРѕ СЃРѕР±С‹С‚РёСЋ onChange) Рё РёР·РјРµРЅСЏРµС‚ СЃРѕРґРµСЂР¶РёРјРѕРµ Р·Р°РІРёСЃРёРјРѕРіРѕ СЃРµР»РµРєС‚Р° РЅР° РѕСЃРЅРѕРІР°РЅРёРё Р·РЅР°С‡РµРЅРёСЏ РІС‹Р±СЂР°РЅРЅРѕРіРѕ РІ СЃС‚Р°СЂС€РµРј СЃРµР»РµРєС‚Рµ.
syncList.prototype._sync = function (firstSelectId, secondSelectId)
{
    var firstSelect = document.getElementById(firstSelectId);
    var secondSelect = document.getElementById(secondSelectId);

    secondSelect.length = 0; //РѕР±РЅСѓР»СЏРµРј РІС‚РѕСЂРѕР№ (РїРѕРґС‡РёРЅРµРЅРЅС‹Р№) SELECT

    if (firstSelect.length>0)//РµСЃР»Рё РїРµСЂРІС‹Р№ (СЃС‚Р°СЂС€РёР№) SELECT РЅРµ РїСѓСЃС‚
    {
        //РёР· СЃРІРѕР№СЃС‚РІР° dataList, СЃ РґР°РЅРЅС‹РјРё РґР»СЏ Р·Р°РїРѕР»РЅРµРЅРёСЏ РїРѕРґС‡РёРЅРµРЅРЅС‹С… СЃРµР»РµРєС‚РѕРІ, Р±РµСЂРµРј С‚Сѓ С‡Р°СЃС‚СЊ РґР°РЅРЅС‹С…, РєРѕС‚РѕСЂР°СЏ СЃРѕРѕС‚РІРµС‚СЃС‚РІСѓРµС‚ РёРјРµРЅРЅРѕ Р·РЅР°С‡РµРЅРёСЋ СЌР»РµРјРµРЅС‚Р°,
        //РІС‹Р±СЂР°РЅРЅРѕРіРѕ РІ РїРµСЂРІРѕРј СЃРµР»РµРєС‚Рµ, Рё РѕРїСЂРµРґРµР»СЏРµС‚ СЃРѕРґРµСЂР¶РёРјРѕРµ РїРѕРґС‡РёРЅРµРЅРЅРѕРіРѕ СЌР»РµРјРµРЅС‚Р° SELECT.
        var optionData = this.dataList[ firstSelect.options[firstSelect.selectedIndex==-1 ? 0 : firstSelect.selectedIndex].value ];
        //Р·Р°РїРѕР»РЅСЏРµРј РІС‚РѕСЂРѕР№ (РїРѕРґС‡РёРЅРµРЅРЅС‹Р№) СЃРµР»РµРєС‚ Р·РЅР°С‡РµРЅРёСЏРјРё (СЃРѕР·РґР°РµРј СЌР»РµРјРµРЅС‚С‹ option)
        for (var key in optionData || null) secondSelect.options[secondSelect.length] = new Option(optionData[key], key);

        //РµСЃР»Рё РІ СЃС‚Р°СЂС€РµРј SELECT-Рµ РЅРµС‚ РІС‹РґРµР»РµРЅРЅРѕРіРѕ РїСѓРЅРєС‚Р°, РІС‹РґРµР»СЏРµРј РїРµСЂРІС‹Р№
        if (firstSelect.selectedIndex == -1) setTimeout( function(){ firstSelect.options[0].selected = true;}, 1 );
        //РµСЃР»Рё РІРѕ РІС‚РѕСЂРѕРј СЃРїРёСЃРєРµ РЅРµС‚ РІС‹РґРµР»РµРЅРЅРѕРіРѕ РїСѓРЅРєС‚Р°, РІС‹РґРµР»СЏРµРј РїРµСЂРІС‹Р№ РµРіРѕ РїСѓРЅРєС‚
        if (secondSelect.length>0) setTimeout( function(){ secondSelect.options[0].selected = true;}, 1 );
    }
    //РµСЃР»Рё РІС‚РѕСЂРѕР№ (РїРѕРґС‡РёРЅРµРЅРЅС‹Р№) СЃРµР»РµРєС‚ РёРјРµРµС‚ РІ СЃРІРѕСЋ РѕС‡РµСЂРµРґСЊ СЃРІРѕРё РїРѕРґС‡РёРЅРµРЅРЅС‹Рµ СЃРµР»РµРєС‚С‹ (С‚Рµ, РґР»СЏ РєРѕС‚РѕСЂС‹С… РѕРЅ РіР»Р°РІРЅС‹Р№),
    //С‚Рѕ Р·Р°РїСѓСЃРєР°РµРј РµРіРѕ РѕР±СЂР°Р±РѕС‚С‡РёРє onchange, С‡С‚РѕР±С‹ РёР·РјРµРЅРёС‚СЊ РµРіРѕ РїРѕРґС‡РёРЅРµРЅРЅС‹Рµ СЃРµР»РµРєС‚С‹
    secondSelect.onchange && secondSelect.onchange();
};