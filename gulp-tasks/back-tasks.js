var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var rename = require('gulp-rename');
var svgstore = require('gulp-svgstore');
var cheerio = require('gulp-cheerio');
var concat = require('gulp-concat');
var cssnano = require('gulp-cssnano');
var include = require("gulp-include");
var del = require('del');
var gulpUglify = require('gulp-uglify-es').default;

gulp.task('style:back', function () {
	gulp.src('./app/scss/main.scss')
		.pipe(plumber())
		.pipe(sass())
		.pipe(postcss([
			autoprefixer({ remove: false })
		]))
		.pipe(gulp.dest('app/css'))
		.pipe(cssnano())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest('./back/css'))
});

gulp.task('clean:back', function () {
	return del.sync('back');
});

gulp.task('sprite:back', function () {
	return gulp.src('./app/img/icons-svg/icon-*.svg')
		.pipe(cheerio({
			run: function ($) {
				$("svg").attr("style", "display: none");
				$("[fill]").removeAttr("fill");
				$("path").removeAttr("class");
				$("path").removeAttr("fill");
				$("path").removeAttr("style");
				$("style").remove();
			},
			parserOptions: {
				xmlMode: true
			}
		}))
		.pipe(svgstore({
			inlineSvg: true
		}))
		.pipe(rename('sprite.svg'))
		.pipe(gulp.dest('./back/svg'));
});

gulp.task('js:back', function () {
	gulp.src(["./app/js/main.js"])
		.pipe(include())
		.pipe(gulpUglify())
		.pipe(concat('main.min.js'))
		.pipe(gulp.dest('./back/js'));
});

gulp.task('build:back', ['clean:back', 'style:back', 'js:back', 'sprite:back'], function () {

});