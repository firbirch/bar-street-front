var gulp = require('gulp');
var path = require('path');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var browserSync  = require('browser-sync');
var rename = require('gulp-rename');
var svgstore = require('gulp-svgstore');
var cheerio = require('gulp-cheerio');
var concat = require('gulp-concat');
var uglify = require('gulp-uglifyjs');
var cssnano = require('gulp-cssnano');
var del = require('del');
var gulpData = require('gulp-data');
var notify = require('gulp-notify');
var fs = require('fs');

var twig = require('gulp-twig');
var imagemin = require('gulp-imagemin');

var requireDir = require('require-dir');

requireDir('./gulp-tasks');

var $ = {
    gutil: require('gulp-util'),
    svgSprite: require('gulp-svg-sprite')
};

gulp.task('twig', function () {
    return gulp.src(path.join(__dirname, 'app/templates/pages/*.twig'))
        .pipe(plumber())
        .pipe(gulpData(() => {
            return JSON.parse(fs.readFileSync(path.join(__dirname, 'app/data/all.json')))
        }))
        .pipe(twig().on('error', notify.onError({
            message: "<%= error.message %>",
            title: "Twig Error!"
        })))
        .pipe(gulp.dest(path.join(__dirname, 'app/html')))
        .pipe(browserSync.stream());
});

gulp.task('style', function () {
    gulp.src('app/scss/main.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(postcss([
            autoprefixer(
                { remove: false }
            )
        ]))
        .pipe(gulp.dest('app/css'))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream());
});



gulp.task('style1', function () {
    gulp.src('app/scss/footer.scss')
        .pipe(plumber())
        .pipe(sass())
        .pipe(postcss([
            autoprefixer(
                { remove: false }
            )
        ]))
        .pipe(gulp.dest('app/css'))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css'))
        .pipe(browserSync.stream());
});

gulp.task('sprite', function () {
    return gulp.src('app/img/icons-svg/icon-*.svg')
        .pipe(cheerio({
            run: function ($) {
                $("svg").attr("style", "display: none");
                $("[fill]").removeAttr("fill");
                $("path").removeAttr("class");
                $("path").removeAttr("fill");
                $("path").removeAttr("style");
                $("style").remove();
            },
            parserOptions: {
                xmlMode: true
            }
        }))
        .pipe(svgstore({
            inlineSvg: true
        }))
        .pipe(rename('sprite.svg'))
        .pipe(gulp.dest('app/img'));
});

// Создание SVG спрайта
gulp.task('svgSprite', function () {
    return gulp.src('app/img/icons-svg/*.svg')
        .pipe($.svgSprite({
            shape: {
                spacing: {
                    padding: 5
                }
            },
            mode: {
                css: {
                    dest: "./",
                    layout: "diagonal",
                    sprite: "../img/sprite-css.svg",
                    bust: false,
                    render: {
                        scss: {
                            dest: "../scss/global/sprite-svg.scss",
                            template: "app/scss/templates/_sprite_template.scss"
                        }
                    }
                }
            },
            variables: {
                mapname: "icons"
            }
        }))
        .pipe(gulp.dest('app/img/'));
});

gulp.task('svg-min', function () {
    return gulp.src('img/icons.svg/sprite.svg')
        .pipe(imagemin([imagemin.svgo()]))
        .pipe(gulp.dest('img'));
});

gulp.task('scripts', function () {
    return gulp.src([
        'app/libs/owlcarousel/owl.carousel.min.js'
    ])
        .pipe(concat('libs.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/js'))
});

gulp.task('clean', function () {
    return del.sync('dist');
});

// Настройка Browser-sync (автоперезагрузка)
gulp.task('browser-sync', function() {       // Создаем таск browser-sync
    browserSync({                            // Выполняем browserSync
        server: {                            // Определяем параметры сервера
            baseDir: ["app/html", "app"]     // Директория по умолчанию
        },
        port: 4000,
        notify: false                        // Отключаем уведомления
    });
});

gulp.task('watch', ['browser-sync', 'style', 'sprite', 'twig', 'svg-min'], function () {
	gulp.watch('app/scss/**/*.scss', ['style']);
	gulp.watch('app/templates/**/*.twig', ['twig']);
	gulp.watch('app/img/icons-svg/**/*.svg', ['sprite', 'svg-min']);
});

// Task по умолчнаию
gulp.task('default', ['watch']);